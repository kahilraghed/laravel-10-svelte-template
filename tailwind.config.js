import colors from 'tailwindcss/colors';
import themes from "daisyui/src/theming/themes";

/** @type {import('tailwindcss').Config & {daisyui: import('daisyui').Config}} */
export default {
    content: [
        "./resources/views/**/*.blade.php",
        "./resources/src/**/*.{svelte,js,ts}",
    ],
    theme: {
        extend: {
            colors: {

            }
        },
    },
    daisyui: {
        // themes: [
        //     {
        //         light: {
        //             ...themes['light'],
        //             primary: colors.pink['500'],//#ec4899
        //             "primary-content": 'white',
        //             secondary: colors.purple['500'],//#a855f7
        //             "primary-content": 'white'
        //         },
        //     },
        // ],
    },
    plugins: [
        require('daisyui')
    ],
}
