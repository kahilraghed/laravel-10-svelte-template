export default function createInertiaApp({ id, resolve, setup, progress, page }: {
    id?: string;
    resolve: any;
    setup: any;
    progress?: {};
    page?: any;
}): Promise<any>;
//# sourceMappingURL=createInertiaApp.d.ts.map
