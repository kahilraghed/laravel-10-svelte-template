import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

import { svelte } from "@sveltejs/vite-plugin-svelte";

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/src/main.ts'],
            refresh: true,
        }),
        svelte()
    ],
    resolve: {
        alias: [
            {
                find: '@',
                replacement: '/resources/src/'
            }
        ]
    },
});
