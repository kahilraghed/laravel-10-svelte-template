declare function link(node: HTMLElement, options?: {}): {
    update(newOptions: any): void;
    destroy(): void;
};
export default link;
//# sourceMappingURL=link.d.ts.map
