import { Axios } from "axios";
import ziggyRoute from "ziggy-js";

declare global {
    var axios: Axios;
    var route: typeof ziggyRoute;

}
