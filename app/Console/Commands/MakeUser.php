<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\PromptsForMissingInput;
use Illuminate\Support\Facades\Hash;

use function Laravel\Prompts\password;
use function Laravel\Prompts\text;

class MakeUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:user {--name=} {--email=} {--password=} {--flags=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new user';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->option('name');
        if ($name == null) {
            $name = text(
                label: 'What is the user name?',
                required: true
            );
        }
        
        $email = $this->option('email');
        if ($email == null) {
            $email = text(
                label: 'What is the user email?',
                required: true
            );
        }
        
        if (User::query()->firstWhere('email', $email)) {
            $this->error('Email already exist in database');
            return;
        }
        
        $password = $this->option('password');
        while ($password == null) {
            $password = password(
                label: 'Provide strong password.',
                required: true
            );

            if ($password != password('Confirm passwod', required: true)) {
                $this->warn('password not match');
                $password = null;
            }
        }


        User::query()->create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'flags' => $this->option('flags')
        ]);

        $this->info('User created successfuly');
    }
}
