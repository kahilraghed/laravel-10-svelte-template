import { FormDataConvertible, Method, PreserveStateOption, Progress, Visit } from '@inertiajs/core';
import { SvelteComponent } from 'svelte';
import {HTMLAttributes, SvelteHTMLElements} from 'svelte/elements';


interface BaseInertiaLinkProps {
    as?: keyof SvelteHTMLElements
    data?: Record<string, FormDataConvertible>
    href: string
    method?: Method
    headers?: Record<string, string>
    preserveScroll?: PreserveStateOption
    preserveState?: PreserveStateOption
    replace?: boolean
    only?: string[]
    queryStringArrayFormat?: 'indices' | 'brackets'
}

type Props = BaseInertiaLinkProps & Omit<HTMLAttributes<HTMLAnchorElement>, keyof BaseInertiaLinkProps>;


interface Events {
    focus: void
    blur: void
    click: void
    dblclick: void
    mousedown: void
    mousemove: void
    mouseout: void
    mouseover: void
    mouseup: void
    'cancel-token': void
    before: Visit
    start: Visit
    progress: void
    finish: Visit
    cancel: void
    success: void
    error: void
}
export default class Link extends SvelteComponent<Props, Events> {

}
