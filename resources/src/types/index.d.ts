
export type Timestamps = {
    created_at: string | null
    updated_at: string | null
}


export type Lipstick = {
    id: number
    code: string
    color: string
    opacity: number
    glossy: boolean
} & Timestamps

export type Login = {
    username: string
    password: number
}
