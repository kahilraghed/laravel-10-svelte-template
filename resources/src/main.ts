import { createInertiaApp } from "@/inertia-svelte";
import MainLayout from "./Layouts/MainLayout.svelte";
import './main.scss'
import Swal from "sweetalert2";

Swal.bindClickHandler();

createInertiaApp({
  resolve: name => {
    const pages = import.meta.glob('./Pages/**/*.svelte', { eager: true })
    let page: any = pages[`./Pages/${name}.svelte`]

    return { default: page.default, layout: page.layout === false ? undefined : (page.layout || MainLayout) }
  },
  setup({ el, App, props }) {
    new App({ target: el, props })
  },
})
